import requests
import json
from google.cloud import datastore
import datetime
import time
import random


def create_client(project_id):
    return datastore.Client(project_id)


def add_listing(client, keystring, data, checkin, checkout):
    parent_key = client.key(keystring)
    parent_entity = datastore.Entity(parent_key)
    parent_entity['queried_at'] = datetime.datetime.utcnow()
    parent_entity['query_start_date'] = checkin
    parent_entity['query_end_date'] = checkout

    listing_key = client.key('Listing')
    listing_entity = datastore.Entity(key=listing_key)
    listing_entity.update(data['listing'])

    parent_quote_key = client.key('PricingQuote')
    parent_quote_entity = datastore.Entity(key=parent_quote_key)
    parent_quote_entity['can_instant_book'] = data['pricing_quote']['can_instant_book']
    parent_quote_entity['monthly_price_factor'] = data['pricing_quote']['monthly_price_factor']
    parent_quote_entity['rate_type'] = data['pricing_quote']['rate_type']
    parent_quote_entity['weekly_price_factor'] = data['pricing_quote']['weekly_price_factor']

    total_price_key = client.key('TotalPrice')
    total_price_entity = datastore.Entity(key=total_price_key)
    total_price_entity.update(data['pricing_quote']['price']['total'])
    parent_quote_entity['total_price'] = total_price_entity

    rate_key = client.key('Rate')
    rate_entity = datastore.Entity(key=rate_key)
    rate_entity.update(data['pricing_quote']['rate'])
    parent_quote_entity['rate'] = rate_entity

    rate_w_fee_key = client.key('RateWithFee')
    rate_w_fee_entity = datastore.Entity(key=rate_w_fee_key)
    rate_w_fee_entity.update(data['pricing_quote']['rate_with_service_fee'])
    parent_quote_entity['rate_with_service_fee'] = rate_w_fee_entity

    parent_entity['listing_detail'] = listing_entity
    parent_entity['pricing_quote'] = parent_quote_entity
    client.put(parent_entity)

    return parent_entity.key


payload = {
    'version': '1.1.3',
    '_format': 'for_explore_search_web',
    'items_per_grid': '100',  # 100
    'experiences_per_grid': '20',
    'guidebooks_per_grid': '20',
    'fetch_filters': 'true',
    'supports_for_you_v3': 'true',
    'screen_size': 'small',
    'timezone_offset': '-420',
    'auto_ib': 'false',
    'selected_tab_id': 'home_tab',
    'location': 'New York, NY, United States',
    'adults': '2',
    'guests': '2',
    '_intents': 'p1',
    'key': '',
    'currency': 'USD',
    'locale': 'en'
}

base = datetime.datetime.today()

for day in range(90):
    checkin = base + datetime.timedelta(days=day)
    checkout = checkin + datetime.timedelta(days=1)
    payload['checkin'] = checkin.strftime("%Y-%m-%d")
    payload['checkout'] = checkout.strftime("%Y-%m-%d")

    r = requests.get('https://www.airbnb.com/api/v2/explore_tabs', params=payload)
    data = json.loads(r.text)

    listings = data['explore_tabs'][0]['sections'][0]['listings']

    client = create_client('nwoke-analysis')
    for listing in listings:
        listing_key = add_listing(client, 'AirbnbListing', listing, checkin.strftime("%Y-%m-%d"),
                                  checkout.strftime("%Y-%m-%d"))
        print('Listing {} added.'.format(listing_key.id))

    time.sleep(random.randint(30, 90))
